Drupal.qeditInit = function() {
  $('.form-autocomplete').click(function (){
    this.select();
  });
  $('.ajax-button').each(function () {
    var uri = $('.ajax-save-url').val();
    var wrapper = '#message';
    var qajax = new Drupal.qajax(uri, this, wrapper);
  });
  $('.title, .date').click(function (){
    var title = $(this).text();
    var loadUri= $('.ajax-load-url').val();
    var id = $(this).attr('id').replace(/title-/, '').replace(/date-/, '');
    Drupal.activeEdit(this);
    if($(this).attr('class') == 'title'){
      if(clean_url) var pattern = '?';
      else pattern = '&';
      Drupal.showBody(loadUri + pattern + 'nid='+id+'&width=600', $(this).attr('id'), title);
    }
  });
  $('td .title, td .date').mouseover(function (){
    var p = $(this).parent('td');
    $(p).css('background-color', '#CFC');
  });
  $('td .title, td .date').mouseout(function (){
    var p = $(this).parent('td');
    $(p).css('background-color', 'transparent');
  });

}

Drupal.qajax = function(uri, button, wrapper){
  this.wrapper = wrapper;
  Drupal.redirectFormButton(uri, button, this);
}
Drupal.qajax.prototype.onsubmit = function () {
  this.progress = new Drupal.progressBar('load progress');
  this.progress.setProgress(-1, 'loading...');
  var el = this.progress.element;
  $(el).css({
    width: '28em',
    paddingTop: '10px',
    display: 'none'
  });
  $(this.wrapper).prepend(el);
  $(el).show();
}
Drupal.qajax.prototype.oncomplete = function (data) {
  $(this.progress.element).remove();
  $('.message').each(function (){
    $(this).remove();
  });
  var div = document.createElement('div');
  $(div).addClass('message');
  $(div).html(data);
  $(this.wrapper).append(div);
  return false;
}
Drupal.qajax.prototype.onerror = function (error) {
  alert('An error occurred:\n\n'+ error);
  // Remove progressbar
  $(this.progress.element).remove();
  this.progress = null;
}

Drupal.activeEdit = function(obj) {
  $('#JT').remove();
  $('.message').remove();
  var id = $(obj).attr('id').replace(/title-/, '').replace(/date-/, '');
  var calendarid = 'edit-date-'+id;
  var type = $(obj).attr('class');
  var revert = $(obj).text();
  var content = $(obj).text();
  var textfield = document.createElement('input');
  var calendar = document.createElement('span');
  var saveButton = document.createElement('input');
  var cancelButton = document.createElement('input');
  var br = document.createElement('br');
  if(type == 'title'){
    var size = 45;
  }
  else{
    var size = 20;
  }
  textfield.setAttribute('value', content);
  $(textfield).attr({type: 'text', name: type, size: size}).addClass('textfield');
  $(textfield).attr({type: 'text', name: type, size: size, id: 'edit-date-' + id}).addClass('textfield');
  $(calendar).attr({id: 'edit-date-'+id+'-button'}).addClass("jscalendar-icon").html('...');

  saveButton.setAttribute('type', 'button');
  cancelButton.setAttribute('type', 'button');
  $(saveButton).attr({ value : 'Save'}).addClass('ajax-button').addClass('form-button');
  $(cancelButton).attr({ value : 'Cancel'}).addClass('form-button');
  $(obj)
    .empty()
    .before(textfield).before(br).before(saveButton).before(cancelButton);
  // calendar
  if(type == 'date'){
    $(br).before(calendar);
    Calendar.setup({
     inputField : calendarid,
     ifFormat : '%Y-%m-%d %H:%M:%S',
     button : calendarid + '-button',
     showsTime : true,
     timeFormat : '24'
    });
  }
  $(saveButton).click(function (){
    var c = $(textfield).val();
    var uri = $('.ajax-save-url').val();
    var token = $('.ajax-form-token').val();
    $.post(uri, {type: type, content: c, nid: id, form_token: token}, function(data){
      if(data){
        $(textfield).remove();
        $(saveButton).remove();
        $(cancelButton).remove();
        $(br).remove();
        if(type == 'date')
          $(calendar).remove();
        var message = document.createElement('span');
        $(message).html('saved').addClass('message').css({color: 'red', float: 'right'});
        $('#JT').remove();
        $(obj).html(c).before(message);
      }
      else{
        // error control
        $(obj).empty().html(revert);
      }
    });
  });
  $(cancelButton).click(function (){
    $(textfield).remove();
    $(saveButton).remove();
    $(cancelButton).remove();
    $(br).remove();
    $('#JT').remove();
    if(type == 'date')
      $(calendar).remove();
    $(obj)
      .empty()
      .html(revert);
  });
   
}
Drupal.showBody = function (url, linkId, title){
	var queryString = url.replace(/^[^\?]+\??/,'');
	var params = parseQuery( queryString );
	if(params['link'] !== undefined){
	  $('#' + linkId).bind('click',function(){window.location = params['link']});
	}
	
	$('#'+linkId).after("<div id='JT' style='width:"+params['width']*1+"px'><div id='JT_arrow_right' style='left:"+((params['width']*1)+1)+"px'></div><div id='JT_close_right'>"+title+"</div><div id='JT_copy'><div class='JT_loader'><div></div></div>");//left side
	$('#JT').show();
	$('#JT_copy').load(url);
}

function parseQuery ( query ) {
   var Params = new Object ();
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

if(Drupal.jsEnabled) {
  $(document).ready(Drupal.qeditInit);
}

