
Summary
-------
This module is an alternate interface of drupal content management. 
One drupal big problem when management nodes, you can't batch add 
tags or categories, or edit title and date on fly. This interface 
provide functionality for someone who has adminster nodes permission
to editing on fly.

Basic Functionality
-------------------
Here is feature list of this module:

* you can filter nodes by type, tag, title, category and date
   first, then batch editing it.
* you can edit title and date in the same screen on fly
   (just click title or date, then appear textfield)
* you can preview content when you editing title
* you can doing batch operation like traditional drupal content
   interface, and further you can add tags in batch

Installation
------------
To install this module, do the following:

1. install jscalendar first (download from http://drupal.org/project/jstools)
2. install Quick Edit, enable module.
3. after module enabled, check out url admin/content/qedit, try this quick interface!

Bugs/Features/Patches
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

* 2008/10/08
 - Fix quick edit title issues
 - Fix quick edit date issues
 - Fix mass delete issues

Author
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)

If you use this module, find it useful, and want to send the author
a thank you note, feel free to contact me.

The author can also be contacted for paid customizations of this
and other modules.

